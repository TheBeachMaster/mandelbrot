#include <stdio.h> 
#define UNIVERSAL_CONSTANT 42 
 
/** 
 * hello.c is currently the world's most complex application.
 * We'll never know, will we?
 */

int main(int argc, char const *argv[])
{
    // The answer to life's most fundamental question:
    printf("Well the answer is %i", UNIVERSAL_CONSTANT);
    return 0;
}