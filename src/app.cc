// #include <Magick++.h>
#include <iostream>
#include "../lib/mandelbrot.hpp"
#include <fstream>

#define PNG     1
#define PPM     2
#define ALL     3
// #define CONVERT 4

using namespace std;
using namespace Magick; 

int main(int argc, char const *argv[])
{
    const char helpOpts [] = "help: \n\t\t about:\t\ta simple program to generate a mandelbrot image.\n\t\t params:\t[0]Image Height\t[1]Image Width\t[2]Number of iterations\t\n\t\t\t\t\t\n\t\t -h:\t\tprint this menu\n";
    if(argc != 4)  
    {
        // Worst hack in Command Line history
        if ((argc == 2) && (argv[1][1] == 'h' ) && (argv[1][0] == '-')) {
            
                std::cout << helpOpts << std::endl;
        } 
        else if(argc == 1)
        {     
            MandelbrotGen gen;
            std::cout << "Generating image with default configs" << std::endl;
                #ifndef IMAGETYPE
                std::cout << "The Application Received a broken build variable" << std::endl;
                #elif IMAGETYPE==PNG
                std::cout << "Generating PNGs only" << std::endl;
                    gen.generateImagePNG();
                #elif IMAGETYPE==PPM
                std::cout << "Generating PPMs only" << std::endl;
                    gen.generateImagePPM();
                // #elif IMAGETYPE==CONVERT
                // std::cout << "Generating PNG from PPM only" << std::endl;
                // gen.generateImagePPM();
                // InitializeMagick(*argv);
                // Image image;
                //         // Read a file into image object 

                //         image.read("image.ppm");

                //         // Crop the image to specified size (width, height, xOffset, yOffset)
                //         image.crop( Geometry(1200,1200, 100, 100) );

                //         // Write the image to a file 
                //         image.write( "image.png" );
                    
                #elif IMAGETYPE==ALL
                    gen.generateImagePNG();
                    gen.generateImagePPM();
                #endif
            //default_gen.generateImagePPM();
        } 
        else if(argc > 4)
        {
            std::cout << helpOpts << std::endl;
        }        
    } 
    int imageH,imageW, iterations;

    if (argc == 4) {
    imageH       = atoi(argv[1]); 
    imageW       = atoi(argv[2]);
    iterations   = atoi(argv[3]);
    MandelbrotGen gen(imageH, imageW, iterations);

    #ifndef IMAGETYPE
    std::cout << "The Application Received a broken build variable" << std::endl;
    //  #elif IMAGETYPE==CONVERT
    //     std::cout << "Generating PNG from PPM only" << std::endl;
    //     gen.generateImagePPM();
    //     InitializeMagick(*argv);
    //     Image image;
    //                     // Read a file into image object 
    //     image.read("image.ppm");
    //     // Crop the image to specified size (width, height, xOffset, yOffset)
    //     image.crop( Geometry(imageW, imageH, 100, 100) );
    //     // Write the image to a file 
    //     image.write( "image.png" );
    #elif IMAGETYPE==PNG
        gen.generateImagePNG();
    #elif IMAGETYPE==PPM
        gen.generateImagePPM();
    #elif IMAGETYPE==ALL
        gen.generateImagePNG();
        gen.generateImagePPM();
    #endif

    }

    return 0;
}